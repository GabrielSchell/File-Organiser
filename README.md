# Files-Organiser
![GitHub last commit](https://img.shields.io/github/last-commit/GabrielSchell/Files-Organiser?color=blue&style=flat-square) ![GitHub top language](https://img.shields.io/github/languages/top/GabrielSchell/Files-Organiser?style=flat-square) ![GitHub language count](https://img.shields.io/github/languages/count/GabrielSchell/Files-Organiser?style=flat-square)

Files-Organiser is python side-project aimed to organised files by sorting them in a dynamically generated folder structure.

## <a name='Tableofcontents'></a> Table of contents
<!-- vscode-markdown-toc -->
* [ Table of contents](#Tableofcontents)
* [Installation](#Installation)
	* [Requirements](#Requirements)
	* [Steps](#Steps)
* [Usage](#Usage)
	* [Running the script](#Runningthescript)
		* [Every platform](#Everyplatform)
		* [Windows](#Windows)
	* [Modify the folder structure](#Modifythefolderstructure)
		* [Example](#Example)
* [Design](#Design)
* [Contribute](#Contribute)
* [License](#License)

<!-- vscode-markdown-toc-config
	numbering=false
	autoSave=true
	/vscode-markdown-toc-config -->
<!-- /vscode-markdown-toc -->


## <a name='Installation'></a>Installation

### <a name='Requirements'></a>Requirements
- Python 3.6 or higher with pip
- A folder full of files to organise ^^

### <a name='Steps'></a>Steps
1. Download or clone the repository
2. Open the terminal in the repository folder
3. Run `pip install -r requirements.txt`

## <a name='Usage'></a>Usage

### <a name='Runningthescript'></a>Running the script

#### <a name='Everyplatform'></a>Every platform
1. Open the terminal in the repository folder
2. Run `python cli.py` for the version in the command line interface or run `python gui.py` for the version with a graphical user interface

#### <a name='Windows'></a>Windows
In the repository folder, double-click on `cli.bat` for the version in the command line interface or `gui.bat` for the version with a graphical user interface. If it closes immediately, [run it in the terminal to see the error message](#Everyplatform).



### <a name='Modifythefolderstructure'></a>Modify the folder structure
You can modify the folder structure by editing the `config.md` file.<br>
The current is a base and I higly suggest that you modify it to fit your needs !

The file is structured as follows:
```markdown
# Name of the root folder
## Name of main category
### Name of sub-category
#### Name of sub-sub-category (etc...)
!A comment
.file_extension
```
#### <a name='Example'></a>Example
The following

```markdown
# 🌌 ORGANISED PLACE:
## Images
.jpg
### Web
!My memes
.png
.gif
### Print
.tiff
## Binaries
!Executable files
.exe
```
would generate the following folder structure:
<br>*(file are only here to show the structure, they are not actually created)*
```markdown
🌌 ORGANISED PLACE
│
├── Images
│   ├── Web
│   │   ├── meme1.png
│   │   ├── meme2.gif
│   │   └── meme3.png
│   └── Print
│       └── landscape.tiff
│
└── Binaries
    ├── program.exe
    └── game.exe
```


## <a name='Design'></a>Design
Objectives for the design of the UI are:

- Minimal
- Typography
- Intuitive
- Fluent design system
- Dark Mode
- Responsive
- Consistent
- Accessible
- Glassmorphism

## <a name='Contribute'></a>Contribute
If you like this project and would like to contribute to make it better, your help is very welcome ! You would be very useful by:
- Reporting a bug
- Submitting a fix
- Discussing the current state of the code
- Proposing new features
- Proposing UI concepts

## <a name='License'></a>License
[Files-Organiser](https://github.com/GabrielSchell/Files-Organiser) © 2020 by [Gabriel Schell](https://pro.gabrielschell.fr) is licensed under [CC BY-NC 4.0](https://creativecommons.org/licenses/by-nc/4.0/)