import os
import sys
import json
import shutil
import contextlib
import colorama

import logger
log = logger.create_logger(__name__)

from colorama import Fore
colorama.init(autoreset=True)

def struct_compiler(s_path="", path="struct.json", log=log) -> dict:
    """Compile the struct.json file into a simple key:values dictionary.
    s_path: the path of the root organised folder.
    path: An optional variable that contains the path of struct.json (or equivalent) file."""

    def ljle(data={}, up="", cs={}, cc={}, lvl=-1, log=log) -> dict:
        """
        local json level explorer: Construct each path of categories based on the JSON levels
        data: the data to explore
        up: path of the folder above the current folder (up: upper path)
        cs: the variable holing the results
        lvl: Level of recursion inside the folder tree.
        """

        # Managing parameters cases
        if type(data) != dict:
            log.print(Fore.RED+"DATA INVALID: The data is not a dictionary.")
            raise TypeError
        elif (data == {}) & (lvl != 0):
            log.print("DATA INVALID: data is empty.")
            raise ValueError

        if type(s_path) != str:
            log.print(Fore.RED+"UPPER PATH INVALID: The upper path is not a string.")
            raise TypeError
        elif (up == "") & (lvl != 0):
            log.print(Fore.RED+"UPPER PATH INVALID: The upper path is empty.")
            raise ValueError

        if type(data) != dict:
            log.print(Fore.RED+"DATA INVALID: The data is not a dictionary.")
            raise TypeError
        elif (cs == {}) & (lvl != 0):
            log.print(Fore.RED+"DATA INVALID: cs is empty.")
            raise ValueError

        if type(data) != dict:
            log.print(Fore.RED+"DATA INVALID: The data is not a dictionary.")
            raise TypeError
        elif (cc == {}) & (lvl != 0):
            log.print(Fore.RED+"DATA INVALID: cc is empty.")
            raise ValueError

        # Setting the variables
        cufn = data.get("sp")  # cufn: current upper folder name
        cufp = "{}{}".format(up, cufn)  # cfp: current upper folder path
        lvl = 0  # Level of recursion inside the folder tree.

        for key in data:
            if key != "sp":
                if type(data[key]) != str and type(data[key]) == list and type(data[key][0]) == dict:
                    lvl += 1
                    ljle(data[key][0], cufp, cs, cc, lvl)
                elif type(data[key]) == str:
                    # cfn: current folder name
                    cfn = os.path.join(cufn, data[key])
                    cfp = "{}{}".format(up, cfn)  # cfp: current folder path
                    cs[key] = cfp
                    cc[key] = data[key]
                    #log.print(Fore.GREEN+"CS: {}".format(json.dumps(cs, indent=4)))
                    #log.print(Fore.GREEN+"CC: {}".format(json.dumps(cc, indent=4)))
                    # log.print("------")
                else:
                    log.print(
                        Fore.RED+"DATA[KEY] INVALID: The data[key] type is incorrect.")
                    raise TypeError
            else:
                #log.print("KEY INVALID: The key is 'sp'.\nKEY SKIPPED\n------")
                pass

        return cs, cc

    # Managing variable cases

    if os.path.exists(s_path) == False:
        log.print(Fore.RED+"PATH INVALID: The path doesn't exist.")
        raise FileNotFoundError
    elif type(s_path) != str:
        log.print(Fore.RED+"PATH INVALID: The path is not a string.")
        raise TypeError
    elif s_path == "":
        log.print(Fore.RED+"PATH INVALID: The path is empty.")
        raise ValueError

    elif os.path.exists(path) == False:
        log.print(Fore.RED+"PATH INVALID: The path doesn't exist.")
        raise FileNotFoundError
    elif type(path) != str:
        log.print(Fore.RED+"PATH INVALID: The path is not a string.")
        raise TypeError
    elif path == "":
        log.print(Fore.RED+"PATH INVALID: The path is empty.")
        raise ValueError

    # Setting the variables
    with open(path, encoding="utf-8", mode='r') as f:
        # struct: variable that contains the struct.json file
        struct = json.loads(f.read())
        compiled_struct = {}  # compiled_struct: the compiled paths in the struct.json file
        compiled_cat = {}  # compiled_cat: the compiled names in the struct.json file

        for cat in struct:
            if type(struct[cat]) != str and type(struct[cat][0]) == dict:
                pass
                compiled_struct, compiled_cat = ljle(
                    struct[cat][0], s_path, compiled_struct, compiled_cat, 0)
            elif type(struct[cat]) == str:
                cfp = os.path.join(s_path, struct[cat]).replace("\\", "/")  # current folder path
                compiled_struct[cat] = cfp
                compiled_cat[cat] = struct[cat]
    f.close()
    #log.print(Fore.GREEN+"COMPILED_STRUCT: {}\n".format(json.dumps(compiled_struct, indent=4)))
    return compiled_struct, compiled_cat

def file_mover(name: str, extensions: str, source: str, destination: str, log=log):
    """
    Move the file from the source to the destination.
    name: the name of the file.
    extensions: the extension of the file.
    source: the initial path of the file.
    destination: the final path of the file.
    """
    dest_path = os.path.join(destination, name + extensions).replace("\\", "/")
    copy = 1

    if len(source) < len(destination):
        # Check if the destination is in a subfolder of the source:
        # if not checked, it will created folder with name of the file and will confuse everything.
        while not os.path.exists(destination):
            os.makedirs(destination)

    if os.path.exists(dest_path):
        try:
            while os.path.exists(dest_path):
                new_name = os.path.join(destination, f"{name}_{str(copy)}{extensions}")
                shutil.move(source, new_name)
                copy += 1
        except:
            log.print("An error occurred while renaming {}.".format(name + extensions))
        else:
            log.print("{} has been renamed to avoid conflicts.".format(name + extensions))
    else:

        shutil.move(source, destination)
        log.print(Fore.LIGHTRED_EX+"{} ->".format(os.path.dirname(source)))
        log.print(Fore.LIGHTGREEN_EX+"{}".format(destination))
    log.print("---")

def estimation(path:str, log=log):
    global of_path
    def count_files(A, counter = 0):
        if os.path.exists(A):
            for fi in os.listdir(A):
                fsp = os.path.join(A, fi).replace("\\", "/")  # fsp: file source path
                if os.path.isfile(fsp):
                    counter += 1
                else:
                    counter=int(count_files(fsp, counter=counter))
        else:
            log.print(Fore.RED+"The {} folder doesn't exist.".format(A))
            raise ValueError
        return counter
    
    with open("./struct.json", encoding="utf-8", mode='r') as f:
        data = json.loads(f.read())
        for key in data:
            of_path = os.path.join(path, data[key][0].get("sp")).replace("\\", "/")  # of_path: organised folder path
    f.close()
        
    count=0
    for filename in os.listdir(path):
        if (os.path.isfile(f"{path}/{filename}")) == True:
            count += 1

    return count+count_files(of_path)

def intialiser(path: str, progress_bar=None, max=0, log=log):
    """Resets all files location in the organised folder into the folder above it.
    path: the path of the folder to organise.
    progress_bar: the progress bar
    fec: track the number of function executed consequently in the GUI to adapt the percentage
    """


    def move_files(A: str, B: str, maxbar=0, counter = 0, log=log):
        """
        Move all the files from the folder A to the folder B.
        """    
        moved_files = 0

        if os.path.exists(A):
            for file in os.listdir(A):
                #log.print("A: ", A)
                #log.print("B: ", B)
                fsp = os.path.join(A, file).replace("\\", "/")  # fsp: file source path
                #log.print("FSP: ", fsp)
                if os.path.isfile(fsp):
                    log.print("{}".format(file))
                    name, extension = os.path.splitext(file)
                    file_mover(name, extension, fsp, B, log=log)
                    counter += 1
                    moved_files += 1
                    if maxbar != 0:
                        progress_bar.setValue(int((counter/maxbar)*100))
                        log.print("{}%".format(int((counter/maxbar)*100)))
                else:
                    #log.print("\nFOLDER: GOING IN ⤵️ \n")
                    counter=int(move_files(fsp, B, maxbar=maxbar, counter=counter, log=log))
                if moved_files == 0:
                    log.print("Already no files")
        else:
            log.print(Fore.RED+"The {} folder doesn't exist.".format(A))
            raise ValueError
        return counter

    def remove_empty_folders(B: str, log=log):
        removed_folders = 0

        with contextlib.suppress(Exception):
            while len(os.listdir(B)) > 0:
                for item in os.listdir(B):
                    current_path = os.path.join(B, item)
                    if len(os.listdir(current_path)) >= 1:
                        remove_empty_folders(current_path, log=log)
                    else:
                        log.print("Parent folder: {}".format(B))
                        log.print("Current folder: {}".format(current_path))
                        os.rmdir(current_path)
                        log.print("Current folder deleted.")
                        log.print("---")
                        removed_folders += 1

        if removed_folders == 0:
            log.print("Already no folders")

    if progress_bar != None:
        maxbar = max
    else:
        maxbar = 0
    
    with open("./struct.json", encoding="utf-8", mode='r') as f:
        data = json.loads(f.read())
        for key in data:
            of_path = os.path.join(path, data[key][0].get("sp")).replace("\\", "/")  # of_path: organised folder path
    f.close()

    log.print("\n------\n0%: Starting initialisation\n------\nMoving files")
    c=move_files(of_path, path, maxbar=maxbar, log=log)
    log.print("50%: Files moved\n------\nRemoving empty folders")
    remove_empty_folders(of_path, log=log)
    log.print("100%: Root cleaned\n------\nIntialisation finished!\n------")
    
    return c

def organiser(folder, progress_bar=None, count=0, max=0, log=log):
    """Organize the files in the folder depending on their extension.
    folder: the path of the folder to organise.
    progress_bar: the progress bar
    fec: track the number of function executed consequently in the GUI to adapt the percentage
    """
    with open('ext.json', encoding="utf-8", mode='r') as ext_file:
        ext_data = json.loads(ext_file.read())
        struct_data, cat_data = struct_compiler(folder.replace("\\", "/")+"/", log=log)
        # fut_ext: future extensions files to implement (the ones not yet implemented)
        fut_ext = []
        fut_ext_null = []

        #log.print(Fore.GREEN+"EXT_DATA: {}".format(json.dumps(ext_data, indent=4)))
        #log.print(Fore.GREEN+"STRUCT_DATA: {}".format(json.dumps(struct_data, indent=4)))
        #log.print(Fore.GREEN+"CAT_DATA: {}".format(json.dumps(cat_data, indent=4)))

        ign = [".driveupload", ".drivedownload"] # Ignore these extensions (gg drive is making the script bug)
        
        counter = 0
        c2=0 # Count files with unkown extensions
        c3_null=0 # Count files with no extensions.

        if progress_bar != None:
            maxbar = max
        else:
            maxbar = 0

        for filename in os.listdir(folder):
            #os.rename( os.path.join(folder,filename).replace("\\","/") , os.path.join(folder,filename.replace("FILE-", "")).replace("\\","/"))
            if os.path.isfile(f"{folder}/{filename}") == True:
                if filename not in ign:
                    name, extension = os.path.splitext(filename)
                    extension=extension.lower()
                    source = os.path.join(folder, filename).replace("\\", "/")
                    category = ext_data.get(extension)
                    pretty_category = cat_data.get(category)
                    dest = struct_data.get(category)
                    log.print("{}{} -".format(name, extension) + Fore.LIGHTBLUE_EX + " {} ".format(pretty_category))
                    if category != None and dest != None:
                        file_mover(name, extension, source, dest, log=log)
                        counter += 1
                        if maxbar != 0:
                            progress_bar.setValue(int(((count+counter)/maxbar)*100))
                            log.print("{}% ({}/{})".format(int(((count+counter)/maxbar)*100), count+counter, maxbar))
                    elif category == None and dest == None:
                        c2+=1
                        log.print(Fore.LIGHTBLUE_EX+"The file extention \"{}\" hasn't been yet implemented".format(extension))
                        log.print("---")
                        if (extension not in fut_ext) and (extension != ""):
                            fut_ext.append(extension)
                        elif (extension not in fut_ext) and (extension == ""):
                            c3_null+=1
                            fut_ext_null.append(extension)
                    elif (category == None and dest != None) or (category != None and dest == None):
                        log.print(Fore.RED+"ERROR IN STRUCT OR EXT\nCHECK \"{}\"=\"{}_CATEGORY\" IN EXT & \"{}_CATEGORY\"=\"PATH\" IN STRUCT\n({}_CATEGORY should be the same in the 2 files)".format(extension,extension.replace(".", ""),extension.replace(".", ""),extension.replace(".", "")))
                        log.print("---")
                        sys.exit(412)


        log.print("\n")

        if counter == 0:
            log.print("The folder is already organized.")
        else:
            log.print("{} files have been moved.".format(counter))

        log.print("------")

        if len(fut_ext) > 0:
            if c2 < 1:
                log.print(Fore.LIGHTBLUE_EX+"No files with unknow file extention(s)")
            else:
                log.print(Fore.LIGHTBLUE_EX+"{} file(s) with unknow file extention(s)".format(c2))
                log.print("The following extensions should be implemented:\n")
                for ext in fut_ext:
                    log.print(Fore.LIGHTYELLOW_EX+ext)
        
        if len(fut_ext_null) > 0:
            log.print(Fore.LIGHTRED_EX+"\n{} file(s) with no extension".format(c3_null))
        
        if (len(fut_ext) == 0) and (len(fut_ext_null) == 0):
            log.print("All extensions encontered have been implemented.")
