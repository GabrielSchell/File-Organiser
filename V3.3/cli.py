import getpass
import os
import sys
from lib import reset, organiser

import colorama

import logger


from colorama import Fore
colorama.init(autoreset=True)

def build_name_of_directory(id_):
    return os.path.join("C:/Users",id_, "Downloads")


def choice_1():
    return build_name_of_directory(
        str(
            input(
                r"Please write down your exact username (you can found it in C:\Users): "
            )
        )
    ).replace("\\", "/")


def choice_2():
    return str(input(r"Please write the exact path of the folder: "))

def stop(pos=""):
    d=""
    if pos == "":
        msg = "You stopped the program"
    elif pos == "end":
        msg = "The program was ending anyway..."
    for i in range(len(msg)):
        d+="-" # decorations
    log.print(Fore.RED+"\n\n{}".format(d))
    log.print(Fore.RED+msg)
    log.print(Fore.RED+"{}\n\n".format(d))
    sys.exit()

def main():
    USER_ID = f"{getpass.getuser()}"
    def_path = build_name_of_directory(USER_ID).replace("\\", "/")

    while True:
        log.print("\nBy default, I clean your download directory but you can use me to clean any folder you want !")
        log.print("I do this by moving files in an organised folder structure based on the extension of the file.")
        log.print("You can:")
        log.print("- modify the organised folder structure in 'config.md'")
        log.print("- stop the program during any interaction by typing 'stop' in the prompt")
        log.print("- kill the program at any time by press 'ctr+c' in the prompt.")

        if input("press 'enter' to continue\n") == "stop":
            stop()
        
        # Question 1.0: Choose the path
        log.print("Where do you want to clean ? (choose by typing the number)")
        log.print("\n1 - (default) Download Folder\n2 - Another folder\n")
        path_choice = input("Your choice: ")
        log.print("------\n")

        if path_choice in ["1","yes",""]:
            log.print(f"Do you confirm that your download directory is {def_path} ? ")
            log.print("\n1 - (default) yes\n2 - no\n")
            yes_or_no_dlf = input("Your choice: ")
            log.print("------\n")
            if yes_or_no_dlf in ["2","no"]:
                path = choice_1()
                while os.path.exists(path) != True:
                    log.print('\nIt appears that the path "', end="")
                    log.print(path, end="")
                    log.print("\" doesn't exist, thus your 'downloads' location was not found. Please retry")
                    log.print("\n --- \n")
                    path = choice_1()
                def_path = path

        elif path_choice == "2":
            path = choice_2()
            while os.path.exists(path) != True:
                log.print('\nIt appears that the path "', end="")
                log.print(path, end="")
                log.print("\" doesn't exist, thus your 'downloads' location was not found. Please retry")
                log.print("\n --- \n")
                path = choice_2()
            def_path = path

        elif path_choice == "stop":
            stop()

        else:
            log.print("That answer was not expected. Please write yes or no")
            log.print("------------")
        
        # Question 2.0: Do you want to update ?
        log.print("Do you want to update the organised folder structure ?")
        log.print("Update is needed everytimes you modify the 'config.md' file.")
        log.print("\n1 - (default) yes\n2 - no\n")
        uq = input("Your choice: ") # update question
        log.print("------\n")
        if uq in ["1","yes",""]:
            log.print("UPDATE: ✅\n\n------------")
            reset(def_path, log=log)
            organiser(def_path, log=log)
        elif uq in ["2", "no"]:
            log.print("UPDATE: ❌\n\n------------")
            organiser(def_path, log=log)
            break
        elif path_choice == "stop":
            stop()
        else:
            log.print("That answer was not expected. Please write yes or no")
            log.print("------------")

    if input("press 'enter' to finish\n") == "stop":
            stop()

if __name__ == "__main__":
    log = logger.create_logger(__name__)
    main()
