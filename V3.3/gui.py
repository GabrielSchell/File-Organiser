import sys, time, json, os.path, sys
from PyQt5.QtWidgets import QApplication, QWidget, QPushButton, QFileDialog, QVBoxLayout, QProgressBar, QLabel
from PyQt5.QtCore import QTimer, QStandardPaths, Qt

from lib import reset, organiser, estimation

import logger
log = logger.create_logger(__name__)

class FolderSelectorApp(QWidget):
    def __init__(self):
        super().__init__()
        self.initUI()

    def initUI(self):
        self.setWindowTitle('Folder Selector')
        self.setGeometry(100, 100, 400, 200)

        # Create buttons
        self.update_button = QPushButton('ORGANISE FOLDER', self)

        # Create a progress bar and a label for "FINISHED!"
        self.progress_bar = QProgressBar(self)
        self.progress_bar.hide()  # Hide the progress bar initially
        self.progress_label = QLabel(self)
        self.progress_label.setAlignment(Qt.AlignCenter)
        self.progress_label.hide()  # Hide the label initially

        # Create a QTimer for showing "FINISHED!" after 1 second
        self.finished_timer = QTimer(self)
        self.finished_timer.timeout.connect(self.show_finished_label)

        # Connect button signals to slots
        self.update_button.clicked.connect(lambda: self.update_folder(log=log))

        # Layout
        layout = QVBoxLayout()
        layout.addWidget(self.update_button)
        layout.addWidget(self.progress_bar)
        layout.addWidget(self.progress_label)
        self.setLayout(layout)

        # Flag to track if the operation is finished
        self.operation_finished = False


    def update_folder(self, log=log):
        folder_path = self.select_folder()
        if folder_path:
            log.print("Selected folder for UPDATE ORGANISED FOLDER:", folder_path)
            max=2*int(estimation(folder_path))
            self.progress_bar.setValue(0)
            self.progress_label.hide()
            self.progress_bar.show()         
            count=reset(folder_path, self.progress_bar, max=max, log=log)
            organiser(folder_path, self.progress_bar, count=count, max=max, log=log)
            self.finished_timer.start(1000)

    def select_folder(self):
        self.progress_label.hide()
        options = QFileDialog.Options()
        default_path = QStandardPaths.writableLocation(QStandardPaths.DownloadLocation)
        folder_dialog = QFileDialog.getExistingDirectory(self, "Select Folder", default_path, options=options)
        return folder_dialog.replace("\\", "/")

    def show_finished_label(self):
        # Update the label to show "FINISHED!" when the QTimer triggers
        self.progress_bar.hide()
        self.progress_label.setText("FINISHED!")
        self.progress_label.show()
        self.operation_finished = True   

if __name__ == '__main__':
    app = QApplication(sys.argv)
    window = FolderSelectorApp()
    window.show()
    sys.exit(app.exec_())
