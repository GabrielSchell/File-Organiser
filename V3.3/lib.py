import os
import sys
import json
import shutil
import pathlib
import contextlib
import colorama

import logger

from colorama import Fore
colorama.init(autoreset=True)


def struct_compiler(s_path="", path="config.md", log=None) -> dict:
    if log == None:
        log = logger.create_logger(__name__)

    """Compile the config.md file into a simple key:values dictionary.
    path: A variable that contains the path of config.md (or equivalent) file."""

    if os.path.exists(s_path) == False:
        log.print(Fore.RED+"PATH INVALID: The path doesn't exist.")
        raise FileNotFoundError
    elif type(s_path) != str:
        log.print(Fore.RED+"PATH INVALID: The path is not a string.")
        raise TypeError
    elif s_path == "":
        log.print(Fore.RED+"PATH INVALID: The path is empty.")
        raise ValueError

    if os.path.exists(path) == False:
        log.print(Fore.RED+"PATH INVALID: The path doesn't exist.")
        raise FileNotFoundError
    elif type(path) != str:
        log.print(Fore.RED+"PATH INVALID: The path is not a string.")
        raise TypeError
    elif path == "":
        log.print(Fore.RED+"PATH INVALID: The path is empty.")
        raise ValueError
    
    log.print("0%: Starting compilation\n------\nParsing the config.md file\n")

    # Compiling the config.md file into
    with open(path, encoding="utf-8", mode='r') as config:
        level = []
        compiled_config = [{}, ""]
        li_c=0 # Line counter
        aex=[] # Already existing extensions
        for line in config:
            li_c+=1
            line=line.replace("\n","")
            log.print("line:[{}] '{}'".format(li_c,line))

            # Check if the line is not a comment and that it's a string
            if (len(line)>1) and (line[0] != "!") and (type(line) == str):
                
                # Calculate the domain of the line
                domain=""
                for l in range(0, len(level)):
                        if level[l] != "":
                            domain+="'{}' -> ".format(level[l])

                # Check if the line contains multiple file extensions
                log.print("CHECKING SYNTAX", end="")
                if line.count(",") > 0:
                    log.print(Fore.RED+"MULTIPLE FILE EXTENSIONS: Only one per line is possible.\nLine:[{}] IN {}{}".format(li_c, domain,line))
                    raise ValueError
                
                # Check if the line contains a '/'
                elif line.count("/") > 0:
                    log.print(Fore.RED+"'/' PRESENCE: Will conflict with path.\nLine:[{}] IN {}{}".format(li_c, domain,line))
                    raise ValueError
                
                # Check for abnormal lines
                elif (line[0] != "#") and (line[0] != "."):
                    log.print(Fore.RED+"UNEXPECTED LINE: Current line doesn't follow the syntax .\nLine:[{}] '{}' (IN {})".format(li_c, line, domain ))
                    raise ValueError
                else:
                    log.print(": OK ")
                
                log.print("PARSING", end="")
                # Parse titles
                if line[0] == "#":
                    log.print(" TITLE")
                    i=0
                    for char in line:
                        if char == "#":
                            i+=1
                        else:
                           break
                    if i == 1:
                        compiled_config[1] = line.replace("#", "").strip()

                    # Remove any previous element under the title
                    log.print("RMV LVL: {}".format(level))
                    for j in range(i, len(level)):
                        #log.print("RMV LVL J: {}".format(level[j]))
                        level.remove(level[j])
                    
                    # Add the title to the list of levels
                    if len(level) <= i-1:
                        level.append(line.replace("#", "").strip())
                    else:    
                        level[i-1] = "{}".format(line.replace("#", "").strip())

                    path=""
                    for k in range(0, len(level)):
                        if level[k] != "":
                            path+=level[k]+"/"
                    path=s_path+path
                    log.print(Fore.LIGHTBLUE_EX+"LEVEL: {}".format(level))
                    log.print(Fore.LIGHTBLUE_EX+"PATH: {}\n".format(path))

                # Parse file extensions
                elif line[0] == ".":
                    log.print(" FILE EXTENSION")
                    clean_line=line.strip()

                    # Check if the file extension is already in the dictionary
                    for k in compiled_config[0].keys():
                        if k == clean_line:
                            log.print(Fore.RED+"ALREADY EXIST: Only one instance of the extension file can exist: {}".format(clean_line))
                            #raise ValueError
                            aex.append(clean_line)
                
                    compiled_config[0][clean_line] = [path, level[-1]]
                    log.print(Fore.LIGHTBLUE_EX+"CONFIG: {}\n".format(compiled_config[0][clean_line][0]))
            elif (len(line)<=1) or (line[0] == "!"):
                log.print("")
            else:
                log.print(Fore.RED+"UNEXPECTED LINE: Current line doesn't follow the syntax .\nLine:[{}] '{}'".format(line, li_c))
                raise ValueError
    if len(aex) > 0:
        log.print(Fore.RED+"MULTIPLE INSTANCES IN CONFIG FILE: Only one instance of these extension files can exist:")
        for a in aex:
            log.print(Fore.RED+"{}".format(a))
        raise ValueError

    log.print("\n------\nCONFIGURATION FILE PARSED\n------\n")
    config.close()
    return compiled_config

def file_mover(name: str, extensions: str, source: str, destination: str, log=None):
    if log == None:
        log = logger.create_logger(__name__)
    """
    Move the file from the source to the destination.
    name: the name of the file.
    extensions: the extension of the file.
    source: the initial path of the file.
    destination: the final path of the file.
    """
    dest_path = os.path.join(destination, name + extensions).replace("\\", "/")
    copy = 1

    if len(os.path.dirname(source)) < len(destination):
        # Check if the destination is in a subfolder of the source:
        # if not checked, it will created folder with name of the file and will confuse everything.
        while not os.path.exists(destination):
            log.print(Fore.LIGHTRED_EX+"Creating {}".format(destination))
            os.makedirs(destination)

    if os.path.exists(dest_path):
        try:
            while os.path.exists(dest_path):
                new_name = os.path.join(destination, f"{name}_{str(copy)}{extensions}")
                shutil.move(source, new_name)
                copy += 1
        except:
            log.print("An error occurred while renaming {}.".format(name + extensions))
        else:
            log.print("{} has been renamed to avoid conflicts.".format(name + extensions))
    else:
        shutil.move(source, dest_path)
        log.print(Fore.LIGHTRED_EX+"{} ->".format(os.path.dirname(source)))
        log.print(Fore.LIGHTGREEN_EX+"{}".format(os.path.dirname(dest_path)))
    log.print("---")

def estimation(path:str, log=None):
    if log == None:
        log = logger.create_logger(__name__)
    global of_path
    def count_files(A, counter = 0):
        if os.path.exists(A):
            for fi in os.listdir(A):
                fsp = os.path.join(A, fi).replace("\\", "/")  # fsp: file source path
                if os.path.isfile(fsp):
                    counter += 1
                else:
                    counter=int(count_files(fsp, counter=counter))
        else:
            log.print(Fore.RED+"The {} folder doesn't exist.".format(A))
            raise ValueError
        return counter
    
    with open(os.path.join(pathlib.Path(__file__).parent.absolute(),'./config.md'), encoding="utf-8", mode='r') as config:
        level = []
        for line in config:
            line = line.replace("\n", "")
            # Check if the line is not a comment and that it's a string
            if (len(line)>0) and (line[0] != "!") and (type(line) == str):

                # Check if the line contains multiple file extensions
                if line.count(",") > 0:
                    domain=""
                    for l in range(0, len(level)):
                            if level[l] != "":
                                domain+="'{}' -> ".format(level[l])
                    log.print(Fore.RED+"MULTIPLE FILE EXTENSIONS: Only one per line is possible.\nIN {}{}".format(domain,line))
                    raise ValueError
                
                # Check if the line contains a '/'
                elif line.count("/") > 0:
                    domain=""
                    for l in range(0, len(level)):
                            if level[l] != "":
                                domain+="'{}' -> ".format(level[l])
                    log.print(Fore.RED+"'/' PRESENCE: Will conflict with path.\nIN {}{}".format(domain,line))
                    raise ValueError 
                
                # Parse titles
                elif line[0] == "#":
                    i=0
                    for char in line:
                        if char == "#":
                            i+=1
                        else:
                           break
                    if i==1:
                        of_path = os.path.join(path,line.replace("#", "").strip()).replace("\\", "/") # of_path: organised folder path
                        break
    config.close()
        
    count=0
    for filename in os.listdir(path):
        if (os.path.isfile(f"{path}/{filename}")) == True:
            count += 1

    return count+count_files(of_path)

def reset(path: str, progress_bar=None, max=0, log=None):
    if log == None:
        log = logger.create_logger(__name__)
    """Resets all files location in the organised folder into the folder above it.
    path: the path of the folder to organise.
    progress_bar: the progress bar
    """

    def move_files(A: str, B: str, maxbar=0, counter = 0, log=None):
        if log == None:
            log = logger.create_logger(__name__)
        """
        Move all the files from the folder A to the folder B.
        """    
        moved_files = 0


        if os.path.exists(A):
            for file in os.listdir(A):
                #log.print("A: ", A)
                #log.print("B: ", B)
                fsp = os.path.join(A, file).replace("\\", "/")  # fsp: file source path
                #log.print("FSP: ", fsp)
                if os.path.isfile(fsp):
                    log.print("{}".format(file))
                    name, extension = os.path.splitext(file)
                    file_mover(name, extension, fsp, B, log=log)
                    counter += 1
                    moved_files += 1
                    if maxbar != 0:
                        progress_bar.setValue(int((counter/maxbar)*100))
                        log.print("{}%".format(round((counter/maxbar)*100,2)))
                else:
                    #log.print("\nFOLDER: GOING IN ⤵️ \n")
                    counter=int(move_files(fsp, B, maxbar=maxbar, counter=counter, log=log))
            if moved_files == 0:
                log.print("Already no files to move in {}.\n---".format(A))
        else:
            log.print(Fore.RED+"The {} folder doesn't exist.".format(A))
            raise ValueError
        return counter

    def remove_empty_folders(B: str, log=None, of_path=None):
        if log == None:
            log = logger.create_logger(__name__)
        removed_folders = 0

        with contextlib.suppress(Exception):
            while len(os.listdir(B)) > 0:
                for item in os.listdir(B):
                    current_path = os.path.join(B, item)
                    if len(os.listdir(current_path)) >= 1:
                        remove_empty_folders(current_path, log=log)
                    else:
                        log.print("Parent folder: {}".format(B))
                        log.print("Current folder: {}".format(current_path))
                        os.rmdir(current_path)
                        log.print("Current folder deleted.")
                        log.print("---")
                        removed_folders += 1

        if removed_folders == 0:
            log.print("Already no folders to remove.")

    if progress_bar != None:
        maxbar = max
    else:
        maxbar = 0


    with open(os.path.join(pathlib.Path(__file__).parent.absolute(),'./config.md'), encoding="utf-8", mode='r') as config:
        level = []
        for line in config:
            line = line.replace("\n", "")
            # Check if the line is not a comment and that it's a string
            if (len(line)>0) and (line[0] != "!") and (type(line) == str):

                # Check if the line contains multiple file extensions
                if line.count(",") > 0:
                    domain=""
                    for l in range(0, len(level)):
                            if level[l] != "":
                                domain+="'{}' -> ".format(level[l])
                    log.print(Fore.RED+"MULTIPLE FILE EXTENSIONS: Only one per line is possible.\nIN {}{}".format(domain,line))
                    raise ValueError
                
                # Check if the line contains a '/'
                elif line.count("/") > 0:
                    domain=""
                    for l in range(0, len(level)):
                            if level[l] != "":
                                domain+="'{}' -> ".format(level[l])
                    log.print(Fore.RED+"'/' PRESENCE: Will conflict with path.\nIN {}{}".format(domain,line))
                    raise ValueError 
                
                # Parse titles
                elif line[0] == "#":
                    i=0
                    for char in line:
                        if char == "#":
                            i+=1
                        else:
                           break
                    if i==1:
                        of_path = os.path.join(path,line.replace("#", "").strip()).replace("\\", "/") # of_path: organised folder path
                        break
    config.close()
    
    log.print("\n------\n0%: Starting reset\n------\nMoving files")
    c=move_files(of_path, path, maxbar=maxbar, log=log)
    log.print("50%: Files moved\n------\nRemoving empty folders")
    remove_empty_folders(of_path, log=log)
    log.print("100%: Root cleaned\n------\nReset finished!\n------")
    
    return c

def organiser(folder, progress_bar=None, count=0, max=0, log=None):
    if log == None:
            log = logger.create_logger(__name__)
    """Organize the files in the folder depending on their extension.
    folder: the path of the folder to organise.
    progress_bar: the progress bar
    fec: track the number of function executed consequently in the GUI to adapt the percentage
    """
    data = struct_compiler(folder.replace("\\", "/")+"/", log=log)
    # fut_ext: future extensions files to implement (the ones not yet implemented)
    fut_ext = []
    fut_ext_null = []

    #log.print(Fore.GREEN+"DATA: {}".format(json.dumps(data, indent=4)))

    ign = [".driveupload", ".drivedownload"] # Ignore these extensions (gg drive is making the script bug)
    
    counter = 0 # Count the number of files
    c2=0 # Count files with unkown extensions
    c3_null=0 # Count files with no extensions.

    if progress_bar != None:
        maxbar = max
    else:
        maxbar = 0

    for filename in os.listdir(folder):
        #os.rename( os.path.join(folder,filename).replace("\\","/") , os.path.join(folder,filename.replace("FILE-", "")).replace("\\","/"))
        if os.path.isfile(f"{folder}/{filename}") == True:
            if filename not in ign:
                name, extension = os.path.splitext(filename)
                extension=extension.lower()
                source = os.path.join(folder, filename).replace("\\", "/")
                data_ext = data[0].get(extension)
                if data_ext != None:
                    dest = data_ext[0]
                    pretty_category = data_ext[1]
                    log.print("{}{} -".format(name, extension) + Fore.LIGHTBLUE_EX + " {} ".format(pretty_category))
                    file_mover(name, extension, source, dest, log=log)
                    counter += 1
                    if maxbar != 0:
                        progress_bar.setValue(int(((count+counter)/maxbar)*100))
                        log.print("{}% ({}/{})".format(round(((count+counter)/maxbar)*100, 2), count+counter, maxbar))
                else:
                    c2+=1
                    log.print(Fore.LIGHTBLUE_EX+"The file extention \"{}\" hasn't been yet implemented".format(extension))
                    log.print("---")
                    if (extension not in fut_ext) and (extension != ""):
                        fut_ext.append(extension)
                    elif (extension not in fut_ext) and (extension == ""):
                        c3_null+=1
                        fut_ext_null.append(extension)


    log.print("\n")

    if counter == 0:
        log.print("The folder is already organized.")
    else:
        log.print("{} files have been moved.".format(counter))

    log.print("------")

    if len(fut_ext) > 0:
        if c2 < 1:
            log.print(Fore.LIGHTBLUE_EX+"No files with unknow file extention(s)")
        else:
            log.print(Fore.LIGHTBLUE_EX+"{} file(s) with unknow file extention(s)".format(c2))
            log.print("The following extensions should be implemented:\n")
            for ext in fut_ext:
                log.print(Fore.LIGHTYELLOW_EX+ext)
    
    if len(fut_ext_null) > 0:
        log.print(Fore.LIGHTRED_EX+"\n{} file(s) with no extension".format(c3_null))
    
    if (len(fut_ext) == 0) and (len(fut_ext_null) == 0):
        log.print("All extensions encontered have been implemented.")
    log.print("\n------")


