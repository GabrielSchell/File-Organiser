# mylogger.py

import logging
import os
import datetime
import re
import builtins
import colorama

from colorama import Fore
colorama.init(autoreset=True)

# Create a custom log level
logging.addLevelName(logging.INFO + 1, 'TEXT')

# Save the built-in print function to a different name
og_print = builtins.print

# Create a log folder if there's none
os.makedirs('logs', exist_ok=True)

def print(self, message="", end="\n", *args, **kws):
    og_print(message, end=end)
    if self.isEnabledFor(logging.INFO + 1):
        # Remove ANSI escape codes from the message
        log_message = re.sub(r'\x1b\[([0-9]{1,2}(;[0-9]{1,2})?)?[mK]', '', message)
        if log_message:
            # Log the message without ANSI escape codes
            self._log(logging.INFO + 1, log_message, args, **kws)
        else:
            # Log an empty line if the message is empty
            self._log(logging.INFO + 1, '', args, **kws)
        # Print the message with ANSI escape codes to the console
        #print(message, end=end)

logging.Logger.print = print

class CustomFormatter(logging.Formatter):
    def format(self, record):
        if record.levelno == logging.INFO + 1:
            fmt = '%(message)s'
        else:
            fmt = '%(asctime)s - %(filename)s - %(levelname)s - %(message)s'
        formatter = logging.Formatter(fmt)
        formatted_message = formatter.format(record)
        # Add ANSI escape codes to the formatted message
        message = record.getMessage()
        ansi_pattern = r'\x1b\[[0-9;]*m'
        ansi_codes = re.findall(ansi_pattern, message)
        for code in ansi_codes:
            formatted_message = formatted_message.replace(code, f'\033[{code[2:]}m')
        return formatted_message

def create_logger(logger_name):
    logger = logging.getLogger(logger_name)
    logger.setLevel(logging.DEBUG)

    # Create file handler
    now = datetime.datetime.now().strftime("%Y-%m-%d_%H-%M-%S")
    log_folder = 'logs'
    os.makedirs(log_folder, exist_ok=True)
    file_path = os.path.join(log_folder, f'{now} # {logger_name}.log')
    if not os.path.exists(file_path):
        file_handler = logging.FileHandler(file_path, encoding='utf-8')
        file_handler.setLevel(logging.DEBUG)
        file_formatter = CustomFormatter()
        file_handler.setFormatter(file_formatter)
        logger.addHandler(file_handler)

        # Log the current date and time at the beginning of the log file
        pre_msg = "\n---\n"
        msg = f'Log started @ {datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S.%f")[:-3]}'
        logger.info(pre_msg)
        og_print(pre_msg)
        og_print(Fore.GREEN+msg)
    return logger
