:: @title: Requirements Generator
:: @description: Generate the requirements.txt file for the project
:: @author: Gabriel Schell
:: @date: 06/04/2024

@echo off
::for %%i in ("%~dp0.") do set "folder0=%%~fi" &:: Get the current folder
for %%i in ("%~dp0..") do set "folder-1=%%~fi" &:: Get the parent folder
set req_file=%folder-1%\requirements.txt

pipreqs %folder-1% --mode no-pin --force

