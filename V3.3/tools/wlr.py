# @title: Wkipedia's list reformatting
# @description: Wikipedia's list reformatting script
# @author: Gabriel Schell
# @date: 25/03/2024

with open("wlr_s.txt", "r") as file:
    for line in file:
        input_str = line.replace('â€“', "-")
        file_ext = input_str.split(" - ")[0].strip("().")
        print("." + file_ext.lower())